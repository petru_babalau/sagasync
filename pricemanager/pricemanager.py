# -*- coding: utf-8 -*-
from __future__ import generators
import os
import json
import requests
import random
import time
import hashlib
import dbmanager
import pickle as dill
from dbfread import DBF
from config import ServerConfig, SECRET_KEY

try:
    from start import logger
except:
    logger = None


FUZZ = False


class Status:
    SUCCESS = 'Success'
    PENDING = 'Pending'
    ERROR = 'Error'


def fuzz():
    if FUZZ:
        time.sleep(random.random())


class Config:
    from config import PathConfig
    PRICE_FILE = os.path.join(PathConfig.docs_dir, 'prices.dat')


Config = Config()


class Record:
    ''' Basic record class '''

    def __init__(self, items):
        ''' Constructor '''
        for name, value in items:
            if name == 'pret_v_tva' and value is None:
                value = 0.0
            setattr(self, name, value)


def create_new_file(file_path):
    open(file_path, 'w').close()


class DatabaseFileNotFound(Exception):
    ''' Custom exception DatabaseFileNotFound '''
    pass


class Price:
    """ Describes a price"""

    def __init__(self, records_dict):
        self.records = records_dict

    @classmethod
    def from_file(cls, file_name):
        ''' Returns a Stock object from pickle.'''
        file_handler = open(file_name, 'rb')
        try:
            records_dict = dill.load(file_handler)
        except EOFError:
            records_dict = {}

        file_handler.close()
        return cls(records_dict=records_dict)

    @classmethod
    def from_dbf(cls, file_name):
        ''' Returns a Stock object from DBF file.'''
        dict_ = dict()
        try:
            records = [rec for rec in file_name if rec.cod not in ['', u'', None]]
            for record in records:
                dict_[record.cod] = (record.pret_v_tva, record.denumire, record.um)
        except TypeError:
            print("DatabaseTableConnectionError")
        return cls(records_dict=dict_)

    def diff_keys(self, price_obj):
        ''' Returns the records' keys that are not in the object passed as an argument. '''
        # return self.records.keys() - price_obj.records_dict.keys()
        return list(set(self.records.keys()).difference(price_obj.records_dict.keys()))

    def duplicate_keys(self, price_obj):
        ''' Returns the intersection of the records' keys.'''
        # return self.records.keys() & price_obj.records.keys()
        return list(set(self.records.keys()).intersection(price_obj.records.keys()))

    def intersection(self, price_obj):
        ''' Returns the intersection of the records' items.'''
        # return self.records.items() & price_obj.records.items()
        return list(set(self.records.items()).intersection(price_obj.records.items()))

    def difference(self, price_obj):
        ''' Returns the records' items that are not in the object passed as an argument. '''
        # return self.records.items() - price_obj.records.items()
        return list(set(self.records.items()).difference(price_obj.records.items()))

    def to_file(self, file_name):
        ''' Saves Stock object in a file as a pickle.'''
        file_handler = open(file_name, 'wb')
        dill.dump(self.records, file_handler)
        file_handler.close()

    def update(self, set_):
        ''' Updates the record dict.'''
        for key, value in set_:
            self.records[key] = value


class NoDatabaseTableNameError(Exception):
    ''' Custom exception NoDatabaseTableNameError '''
    pass


class DatabaseTableConnectionError(Exception):
    ''' Custom exception DatabaseTableConnectionError '''
    pass


class ServerError(Exception):
    ''' Custom exception ServerError '''
    pass



class PriceManager:
    """ Manages the old & the new stocks. """

    def __init__(self, file_name_dbf=None):

        if file_name_dbf is None:
            fuzz()
            logger.info("NoDatabaseTableNameError")
            raise NoDatabaseTableNameError
        self.init(file_name=file_name_dbf)

    def dbf_object_from_table(self, dbf_table_file):
        """ Returns a DBF object from a dbf file. """
        try:
            dbf_obj = DBF(dbf_table_file, recfactory=Record, lowernames=True)
            logger.info("DatabaseTableOk")
            logger.info("Price created from dbf file.")
            return dbf_obj
        except:
            return None

    def init(self, file_name):
        """ Recreates the dbf object, old stock and current stock objects. """
        print("\n****************** Init pricemanager *************************")

        dbf_obj = self.dbf_object_from_table(file_name)
        self.dbf_table_file = file_name

        if dbf_obj is not None:
            try:
                self.old_price = Price.from_file(Config.PRICE_FILE)
                print("Old Stock: from {}".format(Config.PRICE_FILE))
            except IOError:
                self.old_price = Price.from_dbf(dbf_obj)
                create_new_file(Config.PRICE_FILE)
                self.old_price.to_file(Config.PRICE_FILE)
            # logger.info("Stock created from dat file.")
            self.current_price = Price.from_dbf(dbf_obj)
            print("Curr. Stock: from {}".format(file_name))
            print("Diff: ", self.current_price.difference(self.old_price))
            print("****************** End Init pricemanager **********************")
        else:
            print("dbf_obj is None")


    def need_comparing(self):
        if not os.path.isfile(Config.PRICE_FILE):
            create_new_file(Config.PRICE_FILE)
            self.current_price.to_file(Config.PRICE_FILE)
        if PriceManager.file_last_modified(self.dbf_table_file) > PriceManager.file_last_modified(Config.PRICE_FILE):
            return True
        return False

    def get_current_prices(self):
        return self.current_price.records

    def renew_current_price(self):
        print("\n****************** renew_current_price *************************")
        try:
            dbf_obj = self.dbf_object_from_table(self.dbf_table_file)
            self.current_price = Price.from_dbf(dbf_obj)

            print("Curr. Stock: from {}".format(self.dbf_table_file))
            print("Diff: ", self.current_price.difference(self.old_price))
        except:
            print("Database is busy!")
            logger.error("Database is busy!")
        print("****************** End renew_current_price **********************")

    def need_update(self):
        """ Returns True if current stock is different from old stock,
        otherwise returns False."""
        diff = self.current_price.difference(self.old_price)
        if diff:
            #logger.info("Update needed!")
            return True
        #logger.info("Update not needed!")
        return False

    def update_price(self):
        """ Updates the old stock. """
        self.old_price.update(self.current_price.difference(self.old_price))
        self.old_price.to_file(Config.PRICE_FILE)

    def get_adjusted_data(self, data_set):
        """ get_adjusted_data """
        timestamp = time.time()
        token = hashlib.md5()
        token.update(str(SECRET_KEY + str(timestamp)).encode('utf-8'))
        # print(data_set)
        data = {key: {'price': value[0]} for key, value in data_set}
        data['timestamp'] = str(timestamp)
        data['token'] = str(token.hexdigest())
        data = json.dumps(data)
        return data, timestamp

    def send(self, data_set):
        """ Send data to server as JSON. """
        fuzz()
        logger.debug('Start sending data...')
        fuzz()

        data, timestamp = self.get_adjusted_data(data_set)
        data_dict = dict(data_set)
        data_dict['timestamp'] = timestamp

        # print("Send data: ", data)

        headers = {'content-type': 'application/json'}
        try:
            response = requests.post(ServerConfig.server_url, data=data, headers=headers)
            if response.status_code == requests.codes.ok:
                logger.info('Data successfully sent!')
                self.save_data(data_dict, status=Status.SUCCESS)
                self.save_details(data_dict)
            return response
        except requests.exceptions.ConnectionError:
            self.save_data(data_dict, status=Status.ERROR)
            logger.error("Server Connection Error!")
            return None

    def send_2(self, data_set):
        """ Send data to server as JSON. """
        fuzz()
        logger.debug('Start sending data...')
        fuzz()

        data, timestamp = self.get_adjusted_data(data_set)
        data_dict = dict(data_set)
        data_dict['timestamp'] = timestamp

        print("Send data: ", data)

        headers = {'content-type': 'application/json'}
        try:
            response = requests.post(ServerConfig.server_url, data=data, headers=headers)
            if response.status_code == requests.codes.ok:
                logger.info('Data successfully sent!')
            return response
        except:
            raise(ServerError("Server Connection Error!"))

    def save_details(self, data):
        """ Insert into database the information about updated stock. """
        #print("************************************************************************")
        #print(data)
        #print("************************************************************************")
        timestamp = data.pop("timestamp")
        where_clause = ('date={timestamp}'.format(timestamp=timestamp))

        db = dbmanager.DBManager()
        history_id = db.fetch_one(table_name=dbmanager.TABLE_HISTORY_PRICE, where=where_clause)[0]

        query = ('INSERT INTO {table_name}(history_id, jan, new_price, product, measure_unit, old_price) '
                 'VALUES (?, ?, ?, ?, ?, ?)'
            .format(table_name=dbmanager.TABLE_HISTORY_PRICE_DETAILS)
        )

        list_ = []
        for key in data.keys():
            try:
                old_price = self.old_price.records[key][0]
            except KeyError:
                old_price = 0.0
            list_.append((history_id, key, data[key][0], data[key][1], data[key][2], old_price))

        db.execute_many(query, list_)

    def save_data(self, data, status):
        """ Insert into database time and status of the last sent data to server. """
        query = ('INSERT INTO {table_name}(date, status) VALUES ({date}, "{status}")'.format(
            table_name=dbmanager.TABLE_HISTORY_PRICE,
            date=data['timestamp'],
            status=status)
        )
        # (query)
        db = dbmanager.DBManager()
        db.execute_query(query)

    def data_to_send(self):
        return self.current_price.difference(self.old_price)

    @staticmethod
    def file_last_modified(file_name):
        """ Returns the last modified time of the file. """
        return os.path.getmtime(file_name)

    def time_specifications(self):
        print('Table last modified: {}'.format(self.file_last_modified(self.dbf_table_file)))
        print('Stock last modified: {}'.format(self.file_last_modified(Config.PRICE_FILE)))


if __name__ == '__main__':
    pass
