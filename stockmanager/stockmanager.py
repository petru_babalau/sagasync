# -*- coding: utf-8 -*-
from __future__ import generators
import os
import json
import requests
import random
import time
import hashlib
import dbmanager
import pickle as dill
from dbfread import DBF
from config import ServerConfig, SECRET_KEY

try:
    from start import logger
except:
    logger = None


FUZZ = False


class Status:
    SUCCESS = 'Success'
    PENDING = 'Pending'
    ERROR = 'Error'


def fuzz():
    if FUZZ:
        time.sleep(random.random())


class Config:
    from config import PathConfig
    STOCK_FILE = os.path.join(PathConfig.docs_dir, 'stockmanager.dat')


Config = Config()


class Record:
    ''' Basic record class '''

    def __init__(self, items):
        ''' Constructor '''
        for name, value in items:
            if name == 'stoc' and value is None:
                value = 0.0
            setattr(self, name, value)


def create_new_file(file_path):
    open(file_path,'w').close()


class DatabaseFileNotFound(Exception):
    ''' Custom exception DatabaseFileNotFound '''
    pass


class Stock:
    """ Describes a stock"""

    def __init__(self, records_dict):
        self.records = records_dict

    @classmethod
    def from_file(cls, file_name):
        ''' Returns a Stock object from pickle.'''
        file_handler = open(file_name, 'rb')
        try:
            records_dict = dill.load(file_handler)
        except EOFError:
            records_dict = {}

        file_handler.close()
        return cls(records_dict=records_dict)

    @classmethod
    def from_dbf(cls, file_name):
        ''' Returns a Stock object from DBF file.'''
        try:
            records = [rec for rec in file_name if rec.cod not in ['', u'', None]]
        except TypeError:
            raise DatabaseTableConnectionError
        dict_ = dict()
        for record in records:
            dict_[record.cod] = (record.stoc, record.denumire, record.um)
        return cls(records_dict=dict_)

    def compute_total_stock(self):
        ''' Computes the total stock amount.'''
        total = 0.0
        for key, value in self.records.items():
            if float(value[0]) > 0.0:
                total += float(value[0])
        self.set_total_stock(total)
        return total

    def diff_keys(self, stock_obj):
        ''' Returns the records' keys that are not in the object passed as an argument. '''
        # return self.records.keys() - stock_obj.records_dict.keys()
        return list(set(self.records.keys()).difference(stock_obj.records_dict.keys()))

    def duplicate_keys(self, stock_obj):
        ''' Returns the intersection of the records' keys.'''
        # return self.records.keys() & stock_obj.records.keys()
        return list(set(self.records.keys()).intersection(stock_obj.records.keys()))

    def intersection(self, stock_obj):
        ''' Returns the intersection of the records' items.'''
        # return self.records.items() & stock_obj.records.items()
        return list(set(self.records.items()).intersection(stock_obj.records.items()))

    def difference(self, stock_obj):
        ''' Returns the records' items that are not in the object passed as an argument. '''
        # return self.records.items() - stock_obj.records.items()
        return list(set(self.records.items()).difference(stock_obj.records.items()))

    def set_total_stock(self, value):
        ''' '''
        self.total_stock = value

    def to_file(self, file_name):
        ''' Saves Stock object in a file as a pickle.'''
        file_handler = open(file_name, 'wb')
        dill.dump(self.records, file_handler)
        file_handler.close()

    def update(self, set_):
        ''' Updates the record dict.'''
        for key, value in set_:
            self.records[key] = value


class NoDatabaseTableNameError(Exception):
    ''' Custom exception NoDatabaseTableNameError '''
    pass


class DatabaseTableConnectionError(Exception):
    ''' Custom exception DatabaseTableConnectionError '''
    pass


class ServerError(Exception):
    ''' Custom exception ServerError '''
    pass


class StockManager:
    """ Manages the old & the new stocks. """
    def __init__(self, file_name_dbf=None):

        if file_name_dbf is None:
            logger.info("NoDatabaseTableNameError")
            raise NoDatabaseTableNameError
        self.init(file_name=file_name_dbf)

    def dbf_object_from_table(self, dbf_table_file):
        """ Returns a DBF object from a dbf file. """
        try:
            dbf_obj = DBF(dbf_table_file, recfactory=Record, lowernames=True)
            logger.info("DatabaseTableOk")
            logger.info("Stock created from dbf file.")
            return dbf_obj
        except:
            print("Database not found.")
            return None

    def init(self, file_name):
        """ Init the dbf object, old stock and current stock objects. """
        print("\n****************** Init stockmanager *************************")

        dbf_obj = self.dbf_object_from_table(file_name)
        self.dbf_table_file = file_name
        if dbf_obj is not None:
            try:
                self.old_stock = Stock.from_file(Config.STOCK_FILE)
                print("Old Stock: from {}".format(Config.STOCK_FILE))
            except IOError:
                self.old_stock = Stock.from_dbf(dbf_obj)
                create_new_file(Config.STOCK_FILE)
                self.old_stock.to_file(Config.STOCK_FILE)
            # logger.info("Stock created from dat file.")
            self.current_stock = Stock.from_dbf(dbf_obj)
            print("Curr. Stock: from {}".format(file_name))
            print("Diff: ", self.current_stock.difference(self.old_stock))
            print("****************** End Init stockmanager **********************")
        else:
            print("dbf_obj is None")

    def need_comparing(self):
        if not os.path.isfile(Config.STOCK_FILE):
            create_new_file(Config.STOCK_FILE)
            self.current_stock.to_file(Config.STOCK_FILE)
        if StockManager.file_last_modified(self.dbf_table_file) > StockManager.file_last_modified(Config.STOCK_FILE):
            return True
        return False

    def renew_current_stock(self):
        print("\n****************** renew_current_stock *************************")

        try:
            dbf_obj = self.dbf_object_from_table(self.dbf_table_file)
            self.current_stock = Stock.from_dbf(dbf_obj)

            print("Curr. Stock: from {}".format(self.dbf_table_file))
            print("Diff: ", self.current_stock.difference(self.old_stock))
        except:
            print("Database is busy!")
            logger.error("Database is busy!")
        print("****************** End Init stockmanager **********************")

    def get_current_stock(self):
        return self.current_stock.records

    def need_update(self):
        """ Returns True if current stock is different from old stock,
        otherwise returns False."""
        diff = self.current_stock.difference(self.old_stock)
        if diff:
            #logger.info("Update needed!")
            return True
        #logger.info("Update not needed!")
        return False

    def update_stock(self):
        """ Updates the old stock. """
        self.old_stock.update(self.current_stock.difference(self.old_stock))
        self.old_stock.to_file(Config.STOCK_FILE)

    def get_adjusted_data(self, data_set):
        """ get_adjusted_data """
        timestamp = time.time()
        token = hashlib.md5()
        token.update(str(SECRET_KEY + str(timestamp)).encode('utf-8'))
        # print(data_set)
        data = {key: {'stock': value[0]} for key, value in data_set}
        data['timestamp'] = str(timestamp)
        data['token'] = str(token.hexdigest())
        data = json.dumps(data)
        return data, timestamp

    def send(self, data_set):
        """ Send data to server as JSON. """
        fuzz()
        logger.debug('Start sending data...')
        fuzz()

        data, timestamp = self.get_adjusted_data(data_set)
        data_dict = dict(data_set)
        data_dict['timestamp'] = timestamp

        # print("Send data: ", data)

        headers = {'content-type': 'application/json'}
        print(data)
        try:
            response = requests.post(ServerConfig.server_url, data=data, headers=headers)
            if response.status_code == requests.codes.ok:
                logger.info('Data successfully sent!')
                self.save_data(data_dict, status=Status.SUCCESS)
                self.save_details(data_dict)
            return response
        except requests.exceptions.ConnectionError:
            self.save_data(data_dict, status=Status.ERROR)
            logger.error("Server Connection Error!")
            return None

    def send_2(self, data_set):
        """ Send data to server as JSON. """
        fuzz()
        logger.debug('Start sending data...')
        fuzz()

        data, timestamp = self.get_adjusted_data(data_set)
        data_dict = dict(data_set)
        data_dict['timestamp'] = timestamp

        print("Send data: ", data)

        headers = {'content-type': 'application/json'}
        try:
            response = requests.post(ServerConfig.server_url, data=data, headers=headers)
            if response.status_code == requests.codes.ok:
                logger.info('Data successfully sent!')
            return response
        except:
            raise(ServerError("Server Connection Error!"))

    def save_details(self, data):
        """ Insert into database the information about updated stock. """
        #print("************************************************************************")
        #print(data)
        #print("************************************************************************")
        timestamp = data.pop("timestamp")
        where_clause = ('date={timestamp}'.format(timestamp=timestamp))

        db = dbmanager.DBManager()
        history_id = db.fetch_one(table_name=dbmanager.TABLE_HISTORY, where=where_clause)[0]

        query = ('INSERT INTO {table_name}(history_id, jan, new_stock, product, measure_unit, old_stock) '
                 'VALUES (?, ?, ?, ?, ?, ?)'
            .format(table_name=dbmanager.TABLE_HISTORY_DETAILS)
        )

        list_ = []
        for key in data.keys():
            try:
                old_stock = self.old_stock.records[key][0]
            except KeyError:
                old_stock = 0
            list_.append((history_id, key, data[key][0], data[key][1], data[key][2], old_stock))

        db.execute_many(query, list_)

    def save_data(self, data, status):
        """ Insert into database time and status of the last sent data to server. """
        query = ('INSERT INTO {table_name}(date, status) VALUES ({date}, "{status}")'.format(
            table_name=dbmanager.TABLE_HISTORY,
            date=data['timestamp'],
            status=status)
        )
        # (query)
        db = dbmanager.DBManager()
        db.execute_query(query)

    def data_to_send(self):
        return self.current_stock.difference(self.old_stock)

    @staticmethod
    def file_last_modified(file_name):
        """ Returns the last modified time of the file. """
        return os.path.getmtime(file_name)

    def time_specifications(self):
        print('Table last modified: {}'.format(self.file_last_modified(self.dbf_table_file)))
        print('Stock last modified: {}'.format(self.file_last_modified(Config.STOCK_FILE)))


if __name__ == '__main__':
    pass
