import os
import sqlite3
from config import PathConfig


DATABASE = 'sagasync.sqlite'
TABLE_HISTORY = 'history'
TABLE_HISTORY_DETAILS = 'history_details'
TABLE_HISTORY_PRICE = 'history_price'
TABLE_HISTORY_PRICE_DETAILS = 'history_price_details'
STATUSES = ['Pending', 'Success', 'Error']


class DBManager:

    database = DATABASE

    def __init__(self):
        self.con = self.connect()

    def connect(self):
        return sqlite3.connect(os.path.join(PathConfig.main_dir, self.database))

    def fetch_all_list(self, table_name, where=None, order_by=None, limit='20'):
        query = 'SELECT * FROM {table_name}'.format(table_name=table_name)
        if where is not None:
            query += ' WHERE {where}'.format(where=where)
        if order_by is not None:
            query += ' ORDER BY {order_by}'.format(order_by=order_by)
        query += ' LIMIT {limit}'.format(limit=limit)

        results = list()
        try:
            self.con = self.connect()
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()
            rows = cursor.fetchall()
            results = rows
        except sqlite3.Error as e:
            if self.con:
                self.con.rollback()
            print(e)
        finally:
            if self.con:
                self.con.close()
            return results

    def fetch_one(self, table_name, where=None):
        query = 'SELECT * FROM {table_name}'.format(table_name=table_name)
        if where is not None:
            query += ' WHERE {where}'.format(where=where)
        result = None
        try:
            self.con = self.connect()
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()
            result = cursor.fetchone()
        except sqlite3.Error as e:
            if self.con:
                self.con.rollback()
            print(e)
        finally:
            if self.con:
                self.con.close()
            return result

    def execute_query(self, query):
        try:
            self.con = self.connect()
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()
        except sqlite3.Error as e:
            if self.con:
                self.con.rollback()
            print(e)
        finally:
            if self.con:
                self.con.close()

    def execute_many(self, query, list_):
        try:
            self.con = self.connect()
            cursor = self.con.cursor()
            cursor.executemany(query, list_)
            self.con.commit()
        except sqlite3.Error as e:
            if self.con:
                self.con.rollback()
            print(e)
        finally:
            if self.con:
                self.con.close()

    def insert(self, table_name, status='Pending'):
        query = 'INSERT INTO {table_name}(status) VALUES("{status}")'.format(table_name=table_name, status=status)
        self.execute_query(query)