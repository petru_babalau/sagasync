# -*- coding: utf-8 -*-
import os
import wx
import sys
import logging
from frame import *
from config import PathConfig, LogConfig


if hasattr(sys, "frozen"):
    MAIN_DIR = os.path.dirname(sys.executable)
    FULL_REAL_PATH = os.path.realpath(sys.executable)
else:
    SCRIPT_DIR = os.path.dirname(__file__)
    MAIN_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))
    FULL_REAL_PATH = os.path.realpath(sys.argv[0])

IMG_DIR = os.path.join(MAIN_DIR, 'images')


logging.basicConfig(filename=os.path.join(PathConfig.logs_dir, 'access.log'),
                    format=LogConfig.format,
                    datefmt=LogConfig.date_fmt,
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)


class FrameConfig:
    title  = 'SagaSync'
    version = '0.0.5'


if __name__ == '__main__':
    logging.info("-" * 50)
    logging.info("Start application...")
    app = wx.App(None)

    title = ' - '.join([FrameConfig.title, FrameConfig.version])

    frame = MainFrame(None, title=title)
    frame.Show()

    # frame.connectivity_timer_update()

    app.MainLoop()
    logging.info("Application closed!")



