import wx
import os
import logging
import requests
import dbmanager
import stockmanager
import pricemanager
from stockmanager.stockmanager import DatabaseTableConnectionError, DatabaseFileNotFound
from urllib2 import urlopen
from start import logger
from config import LogConfig, ServerConfig, SagaConfig, Options
from wx.lib.buttons import GenBitmapToggleButton
from datetime import datetime
from ObjectListView import ObjectListView, ColumnDefn
from start import IMG_DIR
import thread


STOCK_TIMER = 5000
PRICE_TIMER = 20000


def get_play_icon():
    return 'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJ' \
           'TUUH4QcbChkubrVPCAAADJBJREFUWMO1mXuMXdV1xn/7cc59zMPztMczxh6Dn9gxBgcchIAGQhorahQlgipApKYlSdVCVFE1CQ2K2lQ0' \
           'UlAAFVcqTQtqlELaf2jlulIJasBWoFAGO8YGG4zHM/Z4Hp7HnZk7M/fec/Ze/WOfe22DHQJKr3R0njrn299a37fW3lfxIX/ee7TWAIhI' \
           '5/jkxFZ8sraa+FUuTVu10hgdzRVyuZEqarC3u+OQUmo6ex6l1If63q/99PDgSVav7WdsbKzbpW7X7NzcQ3Ozs6tK5UnmFidJqSLKobzC' \
           'ebAqpqN5OV3tPbS2tI12Le96UGm1t21Z2/j/C8CRE5M5iSrPjo6d2TU0+jaz8THK5l0kqlFJl6glFVKfkKaCE4/D4R1QieiNdrLKbmfd' \
           'ZZtZsXLlns7OjruLhfzcwsICTU1NHx3g5NQEXZ3LOTV86jGr9TfeHj2gji89x5IuUVoYplydo5bW8N4jIjjv8R5EwHtBBJAIEY9XhtxS' \
           'F9ev+DKbV2yV/nXrHu7q7vzWB4X9knde/9/XWdG3spCLond0wfc9/9ZjTFSPMz1/ivlaGZ86AJwI3gteyECCiApAveDwOA9k1xPv6ZYr' \
           'uKX3q1x15Y7BtZf3r/Pee2PMhwN4dmxqYzVdOniqPJA/Mr2HqfIgpco4qfd4CWIRrxHxOC9IJiAvGu8zBpFwTxTeqTAAAZEUKy3c2Ho3' \
           '11/56eq27VdtVEoNXQyHfe+FgwcOsHLlqg3jk2NvnSzvUyPsZ2TuOLVaGe9VAOfA+/BBERDReBFEdHYe3iXnsemzG17AiSX1C/zX1BPM' \
           'H5nLOS8nRWSNUmr4VwI8M3aGrmhlfGjw9YHj0z9Xs60HGZk6SuISnCiceMQrfD2sXhrnEqIYQu1949g7HQaCR8ie9Q7xYVD7J54mR4E4' \
           'Fw2JyLL77rtvbvfu3e8HePuXP0tvTy9vHX372NGJF5sXO1/n9NQRnFNZ6CS81HnSFMSBcwrnQJBGsogKrHrIBOMzgCown5178SHDtOZn' \
           '039P/nArra2tr+zevXuzcx5jgtc2MvPNQ+9wcvDEA0ffefOLs20vMFp6OyhUHI4slD6E1yVCWvNUKuCriiTVeFdnNYATUXgAqas6u5ad' \
           'B4WHB5Ro3p19jeW1jV27H3+81Nzc8kodlwYYGj7JycGh6OTpM391tul5JheHWEzKjRzzPuSRc0KaCrVEaK5dxu9u+Us2tt3C0oSnMluj' \
           'UobaEqQVcDXBO9UAFt6V5aH3GbhzP2dSXpr8N4ZPjj0mIrkLAK5Z3U8tSR8Znz1iynqE+aWzCJCKkHpBfBitF8GnQqVSY2f/HXTEa7h1' \
           'w1e4f9cT3Lbhj1FzRRZmU5JFIalBWvWkqZC6kCLOh7xUolGZhTQ2Bcfm9/HG4MuMj4//bQOgc45Tw8MtU1Nn750pvsTMwgjOC05cGGmj' \
           'BgviFC4BXzH0L98SrkuKpsC2VTdx/+/8iN9efw9qsZ1KKSVdgLSicDWNT02mZIUgpD4lcQk1V6XmqqQ+xRrFofnnOT185u6GSIwxnDwx' \
           'eN3w2DtUO2ZZSuZJEfBhbCKE0iU+C7XCSAEjOVLSzE2zxHeKa9d+kus3fIa9rz/Nq8N7SfUSziRU/BJVt0gqlRBdm+lKgdEKY8AoGC8N' \
           '81s9d+XK8+WvNjUXfqQBSnPzf17OHaNcmQzG6sGLCntPlj+qIQKrYvDmfZ6vMqupJTVu2/IFvr3rH2mt9jIyPM7M2WkqCxV8okAUygWA' \
           'VimsVlijiYwmysMrw3uYLp39hlIm2Ey1VrmlxDEWq3MZoMy7xJ9XxuoqFGKVA9wl27FqtZrBFe7d9dfM+zkeePJuqvkFTNFhIoWJwWqF' \
           'VhqrA3tGK7S2nHIHmZks9QHowRODm6bnzuLyJRIvgfPMiF0mjGAxkh0LRucQuRCgiFCpVKhWq+EcQWmNjWN6lvXxk2++xB996ntUJyBd' \
           'FKSiUU6hAa0URmu0Bm2gwihnz061A+hcFK+ZKU+QOAmFP2OtbgtSV29WX70XFBrJnFkphXOOSqUSbOT8MmUtURRhrSWODJ/7xJ0888Av' \
           '6Fb9JEseaqC8ZCoWjFEYo1n0U1TSJUTkGl1N0+40XcC5GuJ9VgXOsRVc/5zpel9P7gAmSRJqtdr7i/x54KIowkYR1lhW96znPx85zOe2' \
           '/x6LCx7tQCMYDVqBVWCsMF2eANiqa0mtIColTR1CncFMlV4yYJKFWCGiAIOIIU1TkiR5HzitNdbaC7ZzTFpIhe9//Sn+8NPfwgsYpdCZ' \
           'krVVaOPR2jE6OtqttcKLCAke5+ttUaZeVKi/9fYp60ZA4X1yUXAXZe89IKMoBhE+s/MunPdoozFagpqVoE1IqziOnbUmKou3eO8azEm9' \
           'O6kD8+dULF5wklKt/Xrg3gu0vp+YP8Xv//A6mtujkHtWow0Yq9AqRomhs7NzRBdy0XjetmRVQ4X88+C9DnYDOARHqKGCYnTmDFrURUN7' \
           'KeYibTGxIRfn+fdfPMUXvreeqCXFxhBFwWKs1lilMBjaiz0AA3Ziau7d5W0rcTMmtEY+gJJ615yZNAJOYH5hGpUuA+UuKPhKqUvmnY0i' \
           'bJwjl/d8/Yef5NDofpqWQVzQxDmFjcAYQRuFNpom3UXRtnml1Am9bdvmU8taOzFJAY8KDagPfZsnWI/UbQZhdnGaJK2hJboAnDGmEdIG' \
           'ixm4YlOB5177Z278kxW8ObOPlg5FrskQFxQ2VhiTVRKtMNrTQi9dPW0TjW7GmugfemUn2scI5/mfz1otF9Rbmp8kFaGWVsH4CwCeD6oO' \
           'Ml8oMjEzxD2P3MxDe79GvqNCscWSK9aZU1hDqMNGY4xCW0+vvo6O7tajDYBtHcv+ZqXdgvgwAfIStnqNFUILv1hdQICqqzbmW43QRpbY' \
           'Rqg4Ioot1bTCo89+ky89di3DiwdobbfEzYa4IEQ5sDFYq7BWExnVCLHxBTZ13kJXV+93Gy1/ZeitI+uv2M7/HOxmyY40Ot4wOQqzicVq' \
           'mdSniCiW0gXECKRBtXEcE9kIG8XkNfzLvr/jmZd/QK7Z0rHcEBUUUawxVogMWBNKmjEEg9b1vdDnb6J3+brT+Zzd32Bw0027fCHP7dub' \
           '70R8sJJGJ0wI89ziDJkFUiNhfH4MZQ3G5ohsTLFY4OkXHuX2R67mXwd+QLHNUGwRoiJEeUUUCZEFGym0VVgTuhijCaHVGq2XuLLldjqW' \
           'tzRmTQbg6NFf0l7sO1qMiw8Ojh3WJTUGXjcaBARKi9M4l4nFKUzNsXPdbUxXR3jqZw/z+PPf4a2Jl4mahVyzJi5CVM8zq4hM2JustbI6' \
           'A2hCFdFW0V+7kxs2fHFi/fr1n7/oxP344JEdhw69/dre8YeoMBPmsB5Sl3KmdJo08fhE4WqQVjzNtouZ+UniAuTzGpNXxDmwsRBFBmMl' \
           '+JoNYHQWUmv0ecyFEpf3HXy240mu2fGxWzs7ev77gjkJQCVNWLd2y8C2Tdt/urP5S3gxiDM4YLFaxvvQXon26MgTFRQ1O0VLl6LYqsk1' \
           'K/IFRZxXRJHGGoi0JrKBLaMkA2WyfZZ7RtDKcHPro2zYsOGJ88Fdcunj4IEDB154Y8/2/eV/wgiMzY6xkC6E9RUP4rMPKt1IdmsDU/WP' \
           'W6MDcyqo05owMTJGExky5hQawx1rn6S/e+dQ/5rL+mu1GnEcX3rpIxXBKnX1awMDw+pNfdnPSz+mmiyhncJrQWmwGpRWoRM2Ya8zkFqB' \
           'NRlQFUJqNCilg2ptOLZaAZZ7r9/DMnVFKa+bLp8snb0A3Acuv/3y0BuHXz24f8uTx+6nGtVQeLQ2WO0DK6oeqsCG0dlmFNZI6JSNOveM' \
           'BaM0sTE0s4Jv3/YiS4ty3BBv7Ont9hfDoC8F7rnn93PVto9tveHjN/7Fd254lp1tnychsGc1RCo4vzrPLur3bCO/ArPahPBHStAkbGu6' \
           'h69du5fqkn64r7dvfa4Q+Y+0gHn02GE2bdzKuyfeuX5mcu6nbw4OrN43/gxH515E5zyRMYEtpRqmG9Qa1Gnr7CmPlRx9fIqrO+9g8+U7' \
           'jje1NN3V19v76gctYNpfBXDTxq0A1Dwvf/y6HWtODp34yqYz1/zpxOz4loGR/2Bo4TXKeoQlmQYS0BL6dqWJVZGiWkaLX01f/Ak2t99K' \
           'd9fKw23d7Y9cvnrNU3/23T9olMrfyBo1wMDAADt27GCuVFpRKk19f2pm8ebp8cnuJF1qma/OUnELKNFYU6Al30Yxbprv7GmdbG5v37d2' \
           '1eoHlVKnP+y/Corf0E9E+sS7HqWNABPng/E+QevoI733/wBlUh7PkspzSQAAAABJRU5ErkJggg=='

def get_stop_icon():
    return 'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJ' \
           'TUUH4QcbCh0GP2wi9gAADAJJREFUWMO1mHuMHeV5xn/f+30z55y9eHe9i+31bVnfwNhrgxEOlkhTKS6KWkBBKaJpS5UmVCiNaFUpVdRI' \
           'iVolkdo/okqtmqqXtIlQ80f7D6lR1SiQSI0B0wAO2MYGm92Dd2Ev3vVeztmz58x8l/7xzTnYwSBj0ZFGZ2bOSPPM8z7P+z7fKD7k5r1H' \
           'RAAIIfRNT00eyIMazbJsK8r3EwSU1Eyi3y4bVV2/rv9Uqaf3EoBzDq31h3qeut4bqxfHuXn7DuZm5wZW1xpHV2q1b9aXl/fUp6dxlxdI' \
           'sxbUGzjnCD3d5EmCGhhk/dZt9A4OLHR3dX0teP/kzp07pz9SgP/8vSd49HOPMFWdTHLsd2femX5k7vXXuXW8ypb5GVIXaDWb5M0mrtnC' \
           'WYsNAe8dzjnqWYvlm3cxd+QuhvbuZcPwpqeHNgw9NDRw09LExDijoztuHOD8/DxDQ0OMT1S/kkryrZUXX9Aj//scumWRqSmayzVCnpN5' \
           'R/A51ntsUOTO4rzCBY/FYZUieEVLOS498Bl67rknbN8x8g+7Rnd8sdnMKJfTDw+wWp1gurqid+3ferYbdvvv/iOVqRns21PY2hLeeqz3' \
           'OB9wPsRjPNY5rA8473HeY0PxX/vXZtQG1pM9+gV2fuzut8fGxnZfnJlZGxkevn6AJ0+eVBsG+zeFtPxq9+nTQ91PP4OefAd7aZpgHXkI' \
           '5N7jC4A2xN/cO7z3WO9wgcioD7gQAVtnscGD9zSU0LzvATY9/JC9ZceO22/asOHMdQEcv3CRnj5z06WFlfHeF17s3vaLl5SrjuNrdawX' \
           'XHBY5yIjwRUMevI2Y97jg8d6CsAhsugcPsT78hCQ4AjWk917L4Of/zw7R7bs3rxl5MIv4zFXnqytrVGpVHjx5MlX9PGfdW8bryr/xjlc' \
           '5vBB4YLtsOPCL5W3YM4HhfUBG8D6gA8B52zBdLxfEQholBHMMz9moZSSfuGx80uLC0P9A4MLV2KS9sFrb45TqVR4/dy5F5onnh8+OD6u' \
           '3Pk3cK08ltK5K3TlyAu2cu+KUnocsfQ2eHLnokl8LGsefHwpQixdCHhAVELpR//N3H89ycWp6ZMAjdXGewHetnMHZyeqv37+7JnDh8er' \
           'hIlJ/FpWaMmR+Qgq94HcFWV1cc+cJw+Quwgwc1F71gVaHjL/rlEAAuBV1JeogFYl1n3vCc4+f3zbVHXqK13dXVdrMPOOV1970dg1atuf' \
           'PFbeWK3iZqdxQWG967CRF0ZoeU/LO3JP8WDfKbUPIbLV0WPAOx/BAEopVMGiQSEilAFtDM2t26l84+vsGj2wbuPG3lqHwVQ0XdL9uwtn' \
           'Tpc3Ls6Tz1+K4vaxrN5D7iHzsOoCS84z7zwz1jLtPNPWMW090z4wYz1z1jPr4FKxLwZFDVgLAesDBFABtBISBcpolAgDk5MsPP0T6rX5' \
           'v7vKJJcuv6MnJ2b/9fDp13Ezc/jcFvopQBY9rOEdKzYwmxgWtVA3gTwEXDCRKSAEcCEQ4qyGAEYFSqLoySzda2t0E6iIxiiN0QojghFN' \
           'MIGNx5+l+iu/+lng9zoAG3W7d+r8G4zVa9iVlXfBuXYPCzQDrHnPfN4i/61HGBzbz0aTYLTGGIMxBt0+1hpdXIu7RkvC1P/8lFe//GUk' \
           'd4jNix4nBA1BNIkpUbq0wNK5s+bN8fHHt49s+VsBmJ2Z+9yGty7iF2fJbeFK53A4bNGUc2dpOU/N5VgVZZGKkBS7Uaqza6XQgAaU97gs' \
           'JzhLo1bn8swcraUlQrMBWYayGcpaxFpUtopuNTD/+RS1RuuLiS5FBrXIY9vfmWRteZXQGVvgHVH0PjbkVgiseqHfpFQqlQ5rbebax+1I' \
           'pa9g1yQJOi1hBZQIhvhSiVIIAaMCqTKIEtZPVbm0OD8MYM69cbJ3YXq5p3d+kWYriymEQO7iFMi9wwaH9bHd5N6TJAnlcvk9pRURlIrO' \
           'TJKkcz1JEnRiSEtp538RQStBixQ6NJF5ESpzC/h6vR/AmLR/JJuvovIM326uLraHOBkC3heTwwW89yRpQldX11XstR9srtDelQwmaUqS' \
           'ltAFc9EYghZFojQaMKKjpl1OmJ1hfKJ6lymlpaHGygpZ3uqMLFeUtQ0yD+CKcxc85goGtdbXBJckSYfVdgKP9ym0FAC1YCSyZkSjJTKa' \
           'YmBujnptdb9xeV4OLiezeWegd2IUvpNUbIipxQZPWipRqVQ64NpAryzr+0V7LSqWVsVfI4Ipzk3HbEJXo0l3pWvQaCXeK1UYAvKivDG1' \
           'gAW8p9CgxzlPmqaUy+WrTNEGp5T6wPApSqGVxmhItKAViKhYWlEkEjuBN4LzLjeJp2aShMzlRVNutxiPRXVGnGtPFTw6SToMtst4fQsM' \
           'FQFKux1pkk6J2+XWJN6R9XRTqy9NmstrSzOVwSFoxpnrg4szOCh8AOti7vNF8PQedKHBD2Lr/RmMQKJBCvaECK4oeUBwmzZz6NCdP5fb' \
           '9o5NdPX3skROQGGLrGeDJ7PFyHM2xigCwXmarexDg4sAFVpHQ5i2/kRHsEpFoyiB7gqtvj6nlJoUAFVZ93Jrz25aSkXGfMC50Cm3RciD' \
           'I/hAc2kBFTw3somCVIXInG4bomgvohEDWoTa5g0M9Q/NdtKMcvavZ/ePobztxKZ2ksm9wztHCAETIG+sISrcGMAiJKSiSNsMao1WcU+D' \
           'oYRndnQXg4M9pwEkZJabhgZ/wugomdLkOKyzxSInxAgfAhLAXr5MN5B0HnktoOF9rkNFhD5JKLdNUbjYqECiQZsog4Vf+yQ+938GYFRq' \
           'AN555dTpCwt33L6r/LNni+ju8MTAqorYYxt11ivFDx/8DE8Aq0DWFj8qGkDFWSsqGiKVQEWEXm3YbkpsSw3dYki0inFLQWIMRjQp8NbY' \
           'fpKtI2/u27fv5asWTZWS+cTyvZ+aUieOK9eC4KMGVSFv61qUshxQbAYGgVZhTUGhteqMLRHp9DMjmpIIFSP0aU2f1lRESJTGCGjRRewX' \
           '8C0uHD3KyOZN3+40doDgHedfmK0Nj/Z/dn6pNhTOnSagrypaa2EhxiMUCYoKim6EHlH0i6ZPNANiGNCaAW1Yr4V+nbBepwwYw3qt6RND' \
           'RQtlbUhFkejo5pIWRMPZuz9O+PR9Ewdv2//IVYsmJZojvzHG0mV9e/rpB22+dRSPvwJgIKytoQr9pSpQItAt0CfCOq3p14o+LfSJ0KeF' \
           'dTqh32j6TLzeqzVlLZS1kGhFIoX+tIAoVtNepn/zQbZs2PzQ/ffd995VHcD8ynjz5s0bHzd/9DihVH53fqII3nfgttcTqZIiuEZnlnUc' \
           'VSWlKIuipISyCCURUh0DbuyBRSTTghHwSnPiT77EhpHRP98+sv2lY0899cGfPk6feeWfLjx34tHkr76N8jZ+qqhWcR1DUMzOCC4eB4zS' \
           'lEx7hMUGnJp4XyLt7CekxXGiwIvhwte/ir7z7hf37N55V6PRoKur69oMtrf9+w7+wY7DR36QffVPsXia9eXO60iRlHUBTGsQCUWWE0RJ' \
           'MaMFbTQiKjpaQAsYEZQyaAEvmsvf+Q6993zircm3Jg6/8uqrV4F7X4CnTp3iwMEDv7PnjsPftN/6BlnvEAGQEF3X7vzt9UdSpOOY6aLw' \
           'Y8uROPy1xojBiJAohZac1b4B/DPP4G655Xj1/MToJ48eDQcPHLhGc7/GNjY2xms/P8m+Ow5+bd+dh49ueeJfUH/8JazoYkGkCzYg0RpN' \
           'FH6MSzHCxzZDfIkitCZKg8uYefi3Wf7BvzFr88c2D2/6eM9NPeGGPmBOT08zPDzMyRMvDdhE/f3lixcfXv6Pf6fnxz+islQjKSUYMSTa' \
           'kAgYpQvh6whYaxIlJCHgS2Umjhyh/uADrLvl1p9Wmvz+njv2vvWRfKOen3mboU1bOH/+/K3L9fpfLi0u3t888byoZ5+jb2qK8vIiyUqd' \
           'EkIiGpVopL8Xt2499eFNLN55iOaRjzEwMPj0uq6uv9i7d+/xl3/xModuP/TRfUQHOHPmDPv27QPg/IWJR1YbS384s1Lfmtdq/SjVTbOh' \
           'ggtIdxfehkba27M0PNA3s67U8/3RW3b9DcCxY8e4//77+X/fsiy/7nuXspUbfs7/AVNJuDfe0jVVAAAAAElFTkSuQmCC'


class WxTextCtrlHandler(logging.Handler):
    def __init__(self, ctrl):
        # super().__init__()
        logging.Handler.__init__(self)
        self.ctrl = ctrl

    def emit(self, record):
        s = self.format(record) + '\n'
        level_name = record.__dict__.get('levelname', None)
        self.ctrl.SetInsertionPoint(0)
        if level_name == 'ERROR':
            self.ctrl.SetStyle(0, 0, wx.TextAttr(wx.RED))
        elif level_name in ['INFO', 'DEBUG']:
            self.ctrl.SetStyle(0, 0, wx.TextAttr(wx.BLUE))
        self.ctrl.WriteText(s)


LEVELS = [
    logging.DEBUG,
    logging.INFO,
    logging.WARNING,
    logging.ERROR,
    logging.CRITICAL
]


class PanelConfig:
    background_color = wx.Colour(240, 240, 240)


import wx.lib.newevent


HistoryItemSelected, historyEVT_ITEM_SELECTED = wx.lib.newevent.NewEvent()
HistoryItemSelectedCommand, historyEVT_ITEM_SELECTED_COMMAND = wx.lib.newevent.NewCommandEvent()


class HistoryListCtrl(ObjectListView):

    COLS = ['Date', 'Status']

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        ObjectListView.__init__(self, *args, **kwargs)
        cols = []
        for value in self.COLS:
            col = ColumnDefn(value, 'left', 0, value.lower(), isSpaceFilling=True)
            cols.append(col)
        self.SetColumns(cols)
        self.__do_binds()

    def __do_binds(self):
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_list_item_selected)

    def on_list_item_selected(self, event):
        event.Skip()
        index = self.GetFocusedRow()
        id_ = self.GetObjectAt(index)['id']
        # Create the event
        evt = HistoryItemSelected(history_id=id_, type_='history')
        # Post the event
        wx.PostEvent(self.GetParent().GetParent(), evt)

    def populate(self, list_=None, table_name=dbmanager.TABLE_HISTORY):
        if list_ is None:
            db = dbmanager.DBManager()
            results = db.fetch_all_list(table_name, order_by=' date DESC')
        else:
            results = list_

        objects = []
        for item in results:
            objects.append({'id': item[0], 'date': datetime.fromtimestamp(item[1]).strftime('%Y-%m-%d %H:%M:%S'),
                            'status': str(item[2])})

        self.SetObjects(objects)

    def refresh(self):
        self.DeleteAllItems()
        self.populate()


class PricesListCtrl(HistoryListCtrl):

    def refresh(self):
        self.DeleteAllItems()
        self.populate(table_name=dbmanager.TABLE_HISTORY_PRICE)

    def on_list_item_selected(self, event):
        event.Skip()
        index = self.GetFocusedRow()
        id_ = self.GetObjectAt(index)['id']
        # Create the event
        evt = HistoryItemSelected(history_id=id_, type_='history_price')
        # Post the event
        wx.PostEvent(self.GetParent().GetParent(), evt)


DETAILS = {
    'jan': {'is_space_filling': False, 'min_width': 100, 'max_width': 400},
    'product': {'is_space_filling': False, 'min_width': 500, 'max_width': 700},
    'new_stock': {'is_space_filling': False, 'min_width': 150, 'max_width': 200},
    'old_stock': {'is_space_filling': False, 'min_width': 150, 'max_width': 200},
    'new_price': {'is_space_filling': False, 'min_width': 150, 'max_width': 200},
    'old_price': {'is_space_filling': False, 'min_width': 150, 'max_width': 200},
    'measure_unit': {'is_space_filling': False, 'min_width': 200, 'max_width': 300}
}


class DetailsListCtrl(ObjectListView):

    COLS = ['JAN', 'Product', 'New Stock', 'Old Stock', 'New Price', 'Old Price', 'Measure Unit']

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        ObjectListView.__init__(self, *args, **kwargs)
        cols = []
        for value in self.COLS:
            col_value = value.replace(' ', '_').lower()
            col = ColumnDefn(value, 'left', -1, col_value, isSpaceFilling=DETAILS[col_value]['is_space_filling'],
                             minimumWidth=DETAILS[col_value]['min_width'],
                             maximumWidth=DETAILS[col_value]['max_width'])
            cols.append(col)
        self.SetColumns(cols)
        self._type_ = 'history'
        #self.__do_binds()

    def __do_binds(self):
        self.Bind(historyEVT_ITEM_SELECTED, self.on_history_item_changed)

    def on_history_item_changed(self, event):
        history_id = event.history_id
        type_ = event.type_
        self._type_ = type_
        if type_ == 'history':
            results = self.from_history_db(history_id)
        elif type_ == 'history_price':
            results =self.from_history_price_db(history_id)
        #self.populate(results)
        self.refresh(results)

    def from_history_db(self, history_id):
        db = dbmanager.DBManager()
        results = db.fetch_all_list(dbmanager.TABLE_HISTORY_DETAILS,
                                    where=' history_id={history_id}'.format(history_id=history_id))
        return results

    def from_history_price_db(self, history_id):
        db = dbmanager.DBManager()
        results = db.fetch_all_list(dbmanager.TABLE_HISTORY_PRICE_DETAILS,
                                    where=' history_id={history_id}'.format(history_id=history_id))
        return results

    def populate(self, list_=None):
        if list_ is None:
            db = dbmanager.DBManager()
            results = db.fetch_all_list(dbmanager.TABLE_HISTORY_DETAILS)
        else:
            results = list_

        objects = []
        for item in results:
            if self._type_ == 'history':
                objects.append({'jan': str(item[2]), 'product': str(item[3]), 'new_stock': str(item[7]),
                                'old_stock': str(item[6]), 'measure_unit': str(item[5])})
            else:
                objects.append({'jan': str(item[2]), 'product': str(item[3]), 'new_price': str(item[7]),
                                'old_price': str(item[6]), 'measure_unit': str(item[5])})

        self.SetObjects(objects)

    def refresh(self, list_=None):
        self.DeleteAllItems()
        self.populate(list_)


from itertools import islice

def chunks(data, size=50):
    it = iter(data)
    for i in xrange(0, len(data), size):
        yield {k:data[k] for k in islice(it, size)}


class MainPanel(wx.Panel):

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        wx.Panel.__init__(self, *args, **kwargs)
        self.SetBackgroundColour(PanelConfig.background_color)
        self.play_bmp = wx.Bitmap(os.path.join(IMG_DIR, 'play.png'), wx.BITMAP_TYPE_ANY)
        self.stop_bmp = wx.Bitmap(os.path.join(IMG_DIR, 'stop.png'), wx.BITMAP_TYPE_ANY)
        self.logging = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2)
        self.logging_handler = WxTextCtrlHandler(self.logging)
        self.logging_handler.setFormatter(logging.Formatter(LogConfig.format, LogConfig.date_fmt))
        logger.addHandler(self.logging_handler)
        self.__do_layout()
        self.__do_binds()

        self.stock_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self._handle_stock_timer, self.stock_timer)

        self.price_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self._handle_price_timer, self.price_timer)

    def __do_layout(self):
        main_sizer = wx.BoxSizer(wx.VERTICAL)

        top_sizer_h = wx.BoxSizer(wx.HORIZONTAL)

        control_panel = self.__block_control_panel(self)
        top_sizer_h.Add(control_panel, 1)
        top_sizer_h.Add(self.__block_history(self), 2, wx.EXPAND | wx.RIGHT, 3)
        top_sizer_h.Add(self.__block_prices_history(self), 2, wx.EXPAND)

        main_sizer.Add(top_sizer_h, 1, wx.EXPAND)

        self.details = DetailsListCtrl(self, style=wx.LC_REPORT | wx.SUNKEN_BORDER | wx.LC_HRULES | wx.LC_VRULES,
                                       useAlternateBackColors=False)
        main_sizer.Add(self.details, 1, wx.EXPAND | wx.TOP, 4)
        main_sizer.Add(self.logging, 1, wx.EXPAND | wx.TOP, 4)
        self.SetSizerAndFit(main_sizer)

    def __do_binds(self):
        self.play_stop_btn.Bind(wx.EVT_LEFT_UP, self._handle_toggle_play)
        self.logging.Bind(wx.EVT_TEXT, self.on_text)
        self.Bind(historyEVT_ITEM_SELECTED, self.details.on_history_item_changed)
        self.Bind(wx.EVT_BUTTON, self._handle_ping_server, self.ping_server_btn)
        self.Bind(wx.EVT_BUTTON, self._handle_test_database, self.test_database_btn)
        self.sync_all_prices.Bind(wx.EVT_BUTTON, self._handle_sync_all_prices)
        self.sync_entire_stocks.Bind(wx.EVT_BUTTON, self._handle_sync_entire_stocks)

    def _handle_sync_all_prices(self, event):
        print("\n*************** Sync all prices stock ************************")
        self.GetParent().price_manager.renew_current_price()
        stock = self.GetParent().price_manager.get_current_prices()
        CustomDialog(None, data=stock, send_func=self.GetParent().price_manager.send_2,
                     con_func=self.GetParent().get_connection_status).ShowModal()
        event.Skip()

    def _handle_sync_entire_stocks(self, event):
        print("\n*************** Sync entire stock ************************")
        self.GetParent().stock_manager.renew_current_stock()
        stock = self.GetParent().stock_manager.get_current_stock()
        CustomDialog(None, data=stock, send_func=self.GetParent().stock_manager.send_2,
                     con_func=self.GetParent().get_connection_status).ShowModal()
        event.Skip()

    def __block_control_panel(self, parent):
        panel = wx.Panel(parent)
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        sizer_h = wx.BoxSizer(wx.HORIZONTAL)
        self.play_stop_btn = GenBitmapToggleButton(panel, bitmap=self.play_bmp)
        self.play_stop_btn.SetUseFocusIndicator(False)

        # Test buttons (connection)
        self.ping_server_btn = wx.Button(panel, label="Ping server")
        self.test_database_btn = wx.Button(panel, label="Test database")

        sizer_v = wx.BoxSizer(wx.VERTICAL)
        sizer_v.Add(self.ping_server_btn, 1, wx.EXPAND)
        sizer_v.Add(self.test_database_btn, 1, wx.EXPAND)

        sizer_h.Add(self.play_stop_btn, 0, wx.ALL, 3)
        sizer_h.Add(sizer_v, 1, wx.EXPAND)

        self.sync_entire_stocks = wx.Button(panel, label="Sync the entire stock")
        self.sync_entire_stocks.Enable(True)
        self.sync_all_prices = wx.Button(panel, label="Sync all prices")
        self.sync_all_prices.Enable(True)

        main_sizer.Add(sizer_h, 1, wx.EXPAND | wx.TOP, 20)
        main_sizer.Add(self.sync_entire_stocks, 1, wx.EXPAND | wx.TOP | wx.BOTTOM, 5)
        main_sizer.Add(self.sync_all_prices, 1, wx.EXPAND)

        panel.SetSizer(main_sizer)
        return panel

    def __block_history(self, parent):
        panel = wx.Panel(parent)
        # panel.SetBackgroundColour(wx.Colour(100, 100, 0))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.history_list = self.__history_list(panel)
        self.history_list.populate()
        sizer.Add(wx.StaticText(panel, label='Stock'), 0, wx.TOP | wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer.Add(self.history_list, 1, wx.EXPAND | wx.BOTTOM, 2)
        #sizer.Add(self.details, 1, wx.EXPAND)
        panel.SetSizer(sizer)
        return panel

    def __block_prices_history(self, parent):
        panel = wx.Panel(parent)
        # panel.SetBackgroundColour(wx.Colour(100, 100, 0))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.price_history_list = self.__price_history_list(panel)
        self.price_history_list.populate(table_name=dbmanager.TABLE_HISTORY_PRICE)
        sizer.Add(wx.StaticText(panel, label='Price'), 0, wx.TOP | wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer.Add(self.price_history_list, 1, wx.EXPAND | wx.BOTTOM, 2)
        #sizer.Add(self.details, 1, wx.EXPAND)
        panel.SetSizer(sizer)
        return panel

    def __history_list(self, parent):
        list_ctrl = HistoryListCtrl(parent, style=wx.LC_REPORT | wx.SUNKEN_BORDER | wx.LC_HRULES | wx.LC_VRULES,
                                             useAlternateBackColors=False)
        return list_ctrl

    def __price_history_list(self, parent):
        list_ctrl = PricesListCtrl(parent, style=wx.LC_REPORT | wx.SUNKEN_BORDER | wx.LC_HRULES | wx.LC_VRULES,
                                             useAlternateBackColors=False)
        return list_ctrl

    def _handle_ping_server(self, event):
        try:
            data = urlopen(ServerConfig.server_url)
            wx.MessageDialog(None, "Server ok!", "Info", wx.OK).ShowModal()
        except:
            wx.MessageDialog(None, "Server does not respond!\nNo response from: {}".format(ServerConfig.server_url),
                             "Info", wx.OK).ShowModal()
        event.Skip()

    def _handle_test_database(self, event):
        try:
            database_file = open(SagaConfig.table_file, "r+")
            wx.MessageDialog(None, "Database ok!", "Info", wx.OK).ShowModal()
            database_file.close()
        except IOError:
            wx.MessageDialog(None, "Database is busy!\nIt is used by another program!").ShowModal()
        event.Skip()

    def _handle_toggle_play(self, event):
        if self.play_stop_btn.GetToggle():
            self.play_stop_btn.SetBitmapLabel(self.stop_bmp)
            self.__enable_sync_buttons(False)
            self.GetParent().set_running(True)
            self.stock_timer.Start(STOCK_TIMER)
            self.price_timer.Start(PRICE_TIMER)
        else:
            self.play_stop_btn.SetBitmapLabel(self.play_bmp)
            self.GetParent().set_running(False)
            self.__enable_sync_buttons(True)
            self.stock_timer.Stop()
            self.price_timer.Stop()
        event.Skip()

    def __enable_sync_buttons(self, value=False):
        self.sync_entire_stocks.Enable(value)
        self.sync_all_prices.Enable(value)

    def _handle_stock_timer(self, event):
        # print("_handle_stock_timer")
        # self.GetParent().connectivity_timer_update(ServerConfig.server_url)
        thread.start_new_thread(self.GetParent().connectivity_timer_update, (ServerConfig.server_url,))
        thread.start_new_thread(self.GetParent().stock_timer_update, tuple())

    def _handle_price_timer(self, event):
        # print("_handle_stock_timer")
        # thread.start_new_thread(self.GetParent().connectivity_timer_update, (ServerConfig.server_url, ))
        thread.start_new_thread(self.GetParent().price_timer_update, tuple())

    def on_text(self, event):
        event.Skip()
        total_lines = self.logging.GetNumberOfLines()


class MainFrame(wx.Frame):

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        wx.Frame.__init__(self, *args, **kwargs)
        self.connectivity_timer = wx.Timer()
        # self.Bind(wx.EVT_TIMER, self.connectivity_timer_update, self.connectivity_timer)
        self.__do_menu_bar()
        self.panel = MainPanel(self)
        self.CreateStatusBar(2)
        self.Maximize()

        self._running = False
        self.is_connected = False
        # self.check_connectivity()

        try:
            self.create_stock_manager()
        except DatabaseFileNotFound:
            pass

        try:
            self.create_price_manager()
        except DatabaseFileNotFound:
            pass

        # self.check_stocks()

    def get_connection_status(self):
        self.connectivity_timer_update(reference=ServerConfig.server_url)
        return self.is_connected

    def set_running(self, value=False):
        self._running = value

    def get_running(self):
        return self._running

    def create_stock_manager(self):
        try:
            self.stock_manager = stockmanager.StockManager(Options.TABLE_FILE)
        except DatabaseTableConnectionError:
            print("DatabaseTableConnectionError")
        except DatabaseFileNotFound:
            wx.MessageDialog(None, 'DatabaseFileNotFound', 'Error', wx.OK).ShowModal()

    def create_price_manager(self):
        try:
            self.price_manager = pricemanager.PriceManager(Options.TABLE_FILE)
        except DatabaseTableConnectionError:
            print("DatabaseTableConnectionError")
        except DatabaseFileNotFound:
            wx.MessageDialog(None, 'DatabaseFileNotFound', 'Error', wx.OK).ShowModal()

    def __do_menu_bar(self):
        menu_bar = wx.MenuBar()
        file_menu = wx.Menu()
        quit_item = file_menu.Append(wx.ID_EXIT, 'Quit', 'Quit application')
        self.Bind(wx.EVT_MENU, self.on_quit, quit_item)

        tools_menu = wx.Menu()
        options_item = tools_menu.Append(wx.ID_PREFERENCES, 'Options', 'Change options')
        self.Bind(wx.EVT_MENU, self.on_options, options_item)

        menu_bar.Append(file_menu, '&File')
        menu_bar.Append(tools_menu, '&Tools')
        self.SetMenuBar(menu_bar)

    def on_options(self, event):
        event.Skip()
        dlg = OptionsDialog(None, title='Options', size=wx.Size(400, 350)).ShowModal()

    def on_quit(self, event):
        self.Close()

    def check_connectivity(self):
        self.connectivity_timer_update()
        wx.CallLater(5000, self.check_connectivity)

    def connectivity_timer_update(self, reference=ServerConfig.server_url):
        try:
            data = urlopen(reference)
            logger.info("Connected: " + str(reference))
            self.SetStatusText('Connected', 1)
            self.is_connected = True
        except:
            self.SetStatusText('Not connected', 1)
            self.is_connected = False
            logger.error("Not connected: " + str(reference))

    def check_stocks(self):
        self.connectivity_timer_update(ServerConfig.server_url)
        self.stock_timer_update()
        wx.CallLater(5000, self.check_stocks)

    def stock_timer_update(self):
        # print("Running: ", self.get_running())
        # print("Needs comparing: ", self.stock_manager.need_comparing())
        if self.is_connected and hasattr(self, 'stock_manager') and self.get_running() and \
                self.stock_manager.need_comparing():
            # print("In if")
            # self.stock_manager.compare_stocks()
            self.stock_manager.renew_current_stock()

            if self.stock_manager.need_update():
                # print("Update needed!")
                data_to_send = self.stock_manager.data_to_send()
                response = self.stock_manager.send(data_to_send)

                if response is not None:
                    if response.status_code == requests.codes.ok:
                        # print('Success!', response)
                        # logger.info("Success:" + "\tResponse code: " + str(response.status_code))
                        self.stock_manager.update_stock()
                        self.panel.history_list.refresh()
                    else:
                        # print(response)
                        logger.info("Error:" + "\tResponse code: " + str(response.status_code))
                else:
                    # print("Server does not respond.")
                    logger.error("Server does not respond.")
            else:
                print("No update is needed.")
        else:
            # self.create_stock_manager()
            pass

    def price_timer_update(self):
        # print("Running: ", self.get_running())
        # if self.is_connected and hasattr(self, 'price_manager') and self.get_running() and \
        #    self.price_manager.need_comparing():
        if self.is_connected and hasattr(self, 'price_manager') and self.get_running():
            self.price_manager.renew_current_price()

            if self.price_manager.need_update():
                # print("Update needed!")
                data_to_send = self.price_manager.data_to_send()
                response = self.price_manager.send(data_to_send)

                if response is not None:
                    if response.status_code == requests.codes.ok:
                        # print('Success!', response)
                        # logger.info("Success:" + "\tResponse code: " + str(response.status_code))
                        self.price_manager.update_price()
                        self.panel.price_history_list.refresh()
                    else:
                        # print(response)
                        logger.info("Error:" + "\tResponse code: " + str(response.status_code))
                else:
                    # print("Server does not respond.")
                    logger.error("Server does not respond.")
            else:
                print("No update is needed.")
        else:
            # self.create_price_manager()
            pass


class OptionsDialog(wx.Dialog):

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        wx.Dialog.__init__(self, *args, **kwargs)
        sizer = wx.BoxSizer(wx.VERTICAL)
        panel = OptionsPanel(self)
        sizer.Add(panel, 1, wx.EXPAND | wx.ALL, 20)
        self.SetSizer(sizer)
        self.Center()


class OptionsPanel(wx.Panel):

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        wx.Panel.__init__(self, *args, **kwargs)
        # self.SetBackgroundColour(wx.Colour(0, 103, 180))
        self.__do_layout()
        self.__do_binds()

    def __do_layout(self):
        main_sizer = wx.GridBagSizer(vgap=5, hgap=5)

        dbf_label = wx.StaticText(self, label='DBF File: ')
        self.dbf_file_path = wx.TextCtrl(self, value=SagaConfig.table_file)
        self.browse_dbf_btn = wx.Button(self, label='Browse...')

        main_sizer.Add(dbf_label, pos=(0, 0), flag=wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER)
        main_sizer.Add(self.dbf_file_path, pos=(0, 1), span=(1, 3), flag=wx.ALIGN_CENTER_VERTICAL | wx.EXPAND)
        main_sizer.AddGrowableCol(1)
        main_sizer.Add(self.browse_dbf_btn, pos=(0, 4), flag=wx.ALIGN_CENTER_VERTICAL | wx.EXPAND)

        server_label = wx.StaticText(self, label='Server URL: ')
        self.server_input = wx.TextCtrl(self, value=ServerConfig.server_url)
        main_sizer.Add(server_label, pos=(1, 0), flag=wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER)
        main_sizer.Add(self.server_input, pos=(1, 1), flag=wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, span=(1, 4))

        self.save_btn = wx.Button(self, label="Save")
        self.cancel_btn = wx.Button(self, label='Cancel')

        main_sizer.Add((0, 0), pos=(2, 0))
        main_sizer.AddGrowableRow(2)

        main_sizer.Add(self.save_btn, pos=(3, 3), flag=wx.ALIGN_RIGHT | wx.TOP, border=20)
        main_sizer.Add(self.cancel_btn, pos=(3, 4), flag=wx.TOP, border=20)


        self.SetSizerAndFit(main_sizer)

    def __do_binds(self):
        self.save_btn.Bind(wx.EVT_BUTTON, self.on_save_btn)
        self.cancel_btn.Bind(wx.EVT_BUTTON, self.on_cancel_btn)
        self.browse_dbf_btn.Bind(wx.EVT_BUTTON, self.on_browse_dbf_btn)

    def on_browse_dbf_btn(self, event):
        event.Skip()

        dlg = wx.FileDialog(None, defaultDir=os.getcwd(), wildcard = "DBF files (*.dbf)|*.dbf")
        if dlg.ShowModal() == wx.ID_OK:
            file_path = os.path.join(dlg.GetDirectory(), dlg.GetFilename())
            self.dbf_file_path.SetValue(file_path)

    def on_save_btn(self, event):
        event.Skip()
        server_url = self.server_input.GetValue()
        dbf_file = self.dbf_file_path.GetValue()
        ServerConfig.save(server_url)
        SagaConfig.save(dbf_file)
        Options.save(dbf_file, server_url)
        self.GetParent().Destroy()

    def on_cancel_btn(self, event):
        self.GetParent().Destroy()
        event.Skip()


class CustomDialog(wx.Dialog):
    def __init__(self, *args, **kw):
        self.data = kw.pop('data', None)
        self.send_func = kw.pop('send_func', None)
        self.con_func = kw.pop('con_func', None)
        super(CustomDialog, self).__init__(*args, **kw)

        self.chunk_size = 50
        self.chunks = chunks({k: v for k, v in self.data.items()}, self.chunk_size)
        self.data_sent = 0

        self.init_ui()
        self.SetSize((600, 150))
        self.SetTitle("Synchronization")
        self.Center()
        self.timer = wx.Timer()
        self.timer.Bind(wx.EVT_TIMER, self._handle_timer)

        self.Bind(wx.EVT_CLOSE, self.on_close)

    def init_ui(self):
        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        sbs = wx.BoxSizer(wx.VERTICAL)

        self.status = wx.StaticText(pnl, label='Synchronizing')
        self.gauge = wx.Gauge(pnl, range=100, style=wx.GA_HORIZONTAL)
        self.gauge.SetValue(0)

        sbs.Add(self.status, 0, wx.EXPAND | wx.TOP | wx.LEFT, 10)
        sbs.Add(self.gauge, 0, wx.EXPAND | wx.LEFT, 10)
        pnl.SetSizer(sbs)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.start_sync = wx.Button(self, label='Start sync')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(self.start_sync)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox.Add(pnl, proportion=1, flag=wx.ALL | wx.EXPAND, border=5)
        vbox.Add(hbox2, flag=wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, border=10)

        self.SetSizer(vbox)

        self.start_sync.Bind(wx.EVT_BUTTON, self._handle_synchronization)
        closeButton.Bind(wx.EVT_BUTTON, self.on_close)

    def on_close(self, event):
        self.timer.Stop()
        self.Destroy()

    def _handle_synchronization(self, event):
        self.data_sent = 0
        self.timer.Start(35000)
        self.status.SetLabelText('Starting synchronization....')
        self.start_sync.Enable(False)
        event.Skip()

    def _handle_timer(self, event):
        data_len = len(self.data)
        try:
            item = next(self.chunks)
            value = self.gauge.GetValue()
            if self.con_func():
                response = thread.start_new_thread(self.send_func, (item.items(), ))
                self.data_sent += len(item)
                print("Response: {}".format(response))
                value += int(self.chunk_size * 100 / data_len)
                self.gauge.SetValue(value)
                remaining = data_len - self.data_sent
                if remaining < 0:
                    remaining = 0
                self.status.SetLabelText("Synchronizing: Sent: {}.\t Remaining: {}.\t Total: {}."
                                         .format(self.data_sent, remaining, data_len))
            else:
                self.timer.Stop()
                self.status.SetLabelText("Server error. Try later!")
                self.start_sync.Enable(False)
                self.data_sent = 0
        except StopIteration:
            self.gauge.SetValue(100)
            self.stop_timer()
            self.status.SetLabelText("Done!")
            self.data_sent = data_len
            self.start_sync.Enable(False)

        event.Skip()

    def stop_timer(self):
        self.timer.Stop()