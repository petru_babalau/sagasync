# -*- coding: utf-8 -*-
import os
import configparser


DOCS_DIR = 'docs'
TABLE_FILE = 'articole.dbf'
LOGS_DIR = 'logs'
OPTIONS_DIR = 'options'
OPTIONS_FILE = 'options.cfg'
SECRET_KEY= 'a%wIjH1CJ[5g-EY6bwLy4wyg8lpHd"@kT%DYE5bocLk%oRtgB:psu+U#sSGKt#y'


class LogConfig:
    format = "[%(asctime)s] [%(levelname)s] - %(message)s"
    date_fmt = "%d/%m/%Y  %H:%M:%S"


class PathConfig:
    def __init__(self):
        import sys
        if hasattr(sys, "frozen"):
            self.main_dir = os.path.dirname(sys.executable)
        else:
            self.main_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
        self.docs_dir = os.path.join(self.main_dir, DOCS_DIR)
        self.logs_dir = os.path.join(self.main_dir, LOGS_DIR)
        self.options_dir = os.path.join(self.main_dir, OPTIONS_DIR)
        self.options_file = os.path.join(self.options_dir, OPTIONS_FILE)


PathConfig = PathConfig()


class Options:

    def __init__(self):
        self.read()

    def read(self):
        conf = configparser.ConfigParser()
        conf.read(PathConfig.options_file)
        try:
            self.SERVER_URL = conf['SERVER']['SERVER_URL']
        except KeyError:
            self.SERVER_URL = None

        try:
            self.TABLE_FILE = conf['DBF']['DBF_FILE']
        except KeyError:
            self.TABLE_FILE = None

    def save(self, table_file='', server_url=''):
        conf = configparser.RawConfigParser()
        conf.read(PathConfig.options_file)
        conf.set('SERVER', 'SERVER_URL', server_url)
        conf.set('DBF', 'DBF_FILE', table_file)

        with open(PathConfig.options_file, 'w') as configfile:
            conf.write(configfile)

        self.read()


Options = Options()


class SagaConfig:

    table_file = Options.TABLE_FILE

    @staticmethod
    def save(table_file):
        SagaConfig.table_file = table_file


SagaConfig = SagaConfig()


class ServerConfig:

    server_url = Options.SERVER_URL

    @staticmethod
    def save(server_url):
        ServerConfig.server_url = server_url

ServerConfig = ServerConfig()